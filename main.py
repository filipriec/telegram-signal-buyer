
# main.py

import os
import re
import subprocess
from telethon import TelegramClient, events
from telethon.sessions import StringSession
from telethon.tl.types import MessageEntityBotCommand
from dotenv import load_dotenv

load_dotenv()

api_id = os.getenv('API_ID')
api_hash = os.getenv('API_HASH')
phone_number = os.getenv('PHONE_NUMBER')

async def get_session_string():
    session_string = os.getenv('SESSION_STRING')
    if session_string:
        print("Using the default session string from .env file.")
        choice = input("Do you want to fetch a new session string? (0: No, 1: Yes): ")
        if choice == "1":
            async with TelegramClient(StringSession(), api_id, api_hash) as client:
                session_string = client.session.save()
                print("Your new session string is:", session_string)
    else:
        print("No default session string found in .env file.")
        async with TelegramClient(StringSession(), api_id, api_hash) as client:
            session_string = client.session.save()
            print("Your new session string is:", session_string)
    return session_string

async def select_chat(client, prompt):
    default_chat_id = int(os.getenv(prompt.upper() + '_CHAT', '0'))
    if default_chat_id != 0:
        default_chat_title = await get_chat_title(client, default_chat_id)
        print(f"\nSelect the {prompt} chat:")
        print(f"0. Use default ({default_chat_title})")
    else:
        print(f"\nSelect the {prompt} chat:")
        print("0. Use default (no default chat found)")
    print("1. Select from chat list")
    choice = input("Enter your choice (0/1): ")

    if choice == "0":
        if default_chat_id != 0:
            print(f"{prompt.capitalize()} chat: {default_chat_title}, ID: {default_chat_id}")
            return default_chat_id
        else:
            print(f"No default {prompt} chat found.")

    if choice == "1":
        print(f"\nAvailable {prompt} chats:")
        chats = []
        chat_display = ""
        async for dialog in client.iter_dialogs():
            if dialog.is_channel or dialog.is_group or dialog.entity.bot:
                chats.append(dialog)
                chat_display += f"{len(chats)}. {dialog.title} | "
        print(chat_display.strip(' | '))
        choice = int(input("Enter the number of the chat: "))
        if 1 <= choice <= len(chats):
            selected_chat = chats[choice - 1]
            print(f"{prompt.capitalize()} chat: {selected_chat.title}, ID: {selected_chat.id}")
            return selected_chat.id
        else:
            print("Invalid choice.")

    return None

async def get_chat_title(client, chat_id):
    try:
        chat = await client.get_entity(chat_id)
        return chat.title
    except:
        return "Unknown Chat"

async def get_symbol(address):
    try:
        result = subprocess.run(['node', 'getSymbol.js', address], capture_output=True, text=True)
        if result.returncode == 0:
            symbol = result.stdout.strip()
            return symbol if symbol else None
        else:
            print(f"Error fetching symbol for address {address}: {result.stderr}")
            return None
    except Exception as e:
        print(f"Error running getSymbol.js: {e}")
        return None

async def main():
    session_string = await get_session_string()
    client = TelegramClient(StringSession(session_string), api_id, api_hash)
    await client.start(phone=phone_number)

    SOURCE_CHAT_ID = await select_chat(client, "source")
    if SOURCE_CHAT_ID is None:
        print("No source chat selected. Exiting.")
        await client.disconnect()
        return

    TARGET_CHAT_ID = await select_chat(client, "target")
    if TARGET_CHAT_ID is None:
        print("No target chat selected. Exiting.")
        await client.disconnect()
        return

    @client.on(events.NewMessage(chats=SOURCE_CHAT_ID))
    async def handle_new_message(event):
        message = event.message.message
        addresses = re.findall(r'Address: (\w+)', message)
        if addresses:
            if "NEW SELL ALERT" in message:
                print("Sell alert found in the message:")
                for address in addresses:
                    print(address)
                    symbol = await get_symbol(address)
                    if symbol:
                        await client.send_message(TARGET_CHAT_ID, "/sell")
                        await client.send_message(TARGET_CHAT_ID, symbol)
            elif "Drop detected" in message:
                print("Buy alert found in the message:")
                for address in addresses:
                    print(address)
                    # await client.send_message(TARGET_CHAT_ID, "/buy")
                    await client.send_message(TARGET_CHAT_ID, address)
        else:
            print("No addresses found in the message.")

    print("Client started. Waiting for new messages...")
    await client.run_until_disconnected()

if __name__ == '__main__':
    import asyncio
    asyncio.run(main())
