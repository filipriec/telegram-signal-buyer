
# Telegram Bot for Buying Crypto

This code sets up a Telegram bot that monitors messages in a source chat/channel and sends commands to a target bot based on the content of the messages. The bot is designed to automatically buy crypto when certain conditions are met.

## Prerequisites

- Python 3.x
- Node.js
- Telegram API credentials (API ID and API Hash) 

## Setup

1. Clone the repository:

```bash
git clone git@gitlab.com:filipriec/telegram-signal-buyer.git
cd telegram-bot
```

2. Create a virtual environment and activate it:

```bash
python3 -m venv venv
source venv/bin/activate
```

3. Install the required Python packages:

```bash
pip install -r requirements.txt
```

4. Create a `.env` file in the project root and provide the following values:

```
API_ID=<your-api-id>
API_HASH=<your-api-hash>
PHONE_NUMBER=<your-phone-number>
SESSION_STRING=<your-session-string>
SOURCE_CHAT=<source-chat-id>
TARGET_CHAT=<target-chat-id>
```

- To obtain the API ID and API Hash, follow the instructions in this Telegram guide: [How to get Telegram API ID and API Hash](https://core.telegram.org/api/obtaining_api_id)
- The `SESSION_STRING` can be left empty initially. It will be generated automatically when you run the bot for the first time.
- The `SOURCE_CHAT` and `TARGET_CHAT` IDs can be obtained by running the bot and selecting the chats interactively.

5. Run the bot:

```bash
python main.py
```

## Usage

1. Run the `main.py` script.
2. If prompted, select the source and target chats by following the on-screen instructions.
3. The bot will start monitoring the source chat for new messages.
4. When a message containing "SELL ALERT" is detected, the bot will extract the addresses from the message and send the `/sell` command followed by the symbol associated with each address to the target bot.
5. When a message containing "Drop detected" is detected, the bot will extract the addresses from the message and send each address to the target bot.

## Notes

- The code assumes that the target bot is set up to automatically buy crypto based on the commands it receives.
- The `getSymbol.js` script is used to fetch the symbol associated with a given address using the Solana Metaplex library.
- Make sure to handle the bot token and other sensitive information securely and avoid committing them to version control.

Please note that this code is provided as-is and may require additional modifications and error handling based on your specific use case and environment.
