const { Metaplex } = require('@metaplex-foundation/js');
const { Connection, PublicKey } = require('@solana/web3.js');

const CONNECTION_URL = 'https://api.mainnet-beta.solana.com';

async function getSymbol(mintAddress) {
  const connection = new Connection(CONNECTION_URL);
  const metaplex = Metaplex.make(connection);

  try {
    const mintPublicKey = new PublicKey(mintAddress);
    const nft = await metaplex.nfts().findByMint({ mintAddress: mintPublicKey });
    if (nft && nft.json && nft.json.symbol) {
      console.log(nft.json.symbol);
    } else {
      console.log('');
    }
  } catch (error) {
    console.error('Error fetching metadata:', error);
    process.exit(1);
  }
}

const mintAddress = process.argv[2];
getSymbol(mintAddress);
